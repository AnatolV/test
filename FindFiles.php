	class FindFiles{

		public $extension;
		public $path;
		public $mask;

		function __construct ($path = 'files',$extension = 'txt',  $mask = '/[^a-zA-Z0-9]/'){
			$this->extension = $extension;
			$this->path      = $path;
			$this->mask      = $mask;
		}

		public function show (){
			$arFiles = $this->find();
			if ( ! empty($arFiles)){
				natsort($arFiles);
				foreach ($arFiles as $file){
					$name = explode('.',$file) ;
					echo "$name[0]<br>";
				}
			}else{
				echo "Файлов в каталоге $this->path с расширением $this->extension и удовлетворяющих условию $this->mask не найдено";
				exit();
			}

		}

		private function find (){
			$arFiles = array ();
			if (is_dir($this->path)){
				$files = scandir($this->path);
				if (!empty($files)){
					foreach ($files as $file){
						if ($this->checkFile($file)){
							$arFiles[] = $file;
						}
					}
				}else{
					echo "$this->path не содержит файлов";
					exit();
				}
			}else{
				echo "$this->path не каталог";
				exit();
			}

			return $arFiles;
		}

		private function checkFile ($filename){
			$check  = false;
			$arFile = explode('.', $filename);
			if ($arFile[1] === $this->extension){
				if ($this->checkName($arFile[0])){
					$check = true;
				}
			}
			return $check;
		}

		private function checkName ($filename){
			$check = false;
			if (!preg_match($this->mask, $filename)){
				$check = true;
			}
			return $check;
		}
	}