<?php
	final class Init{
		private $connect;

		function __construct (){
			$this->connect = mysqli_connect('localhost', 'root', 'xxx', 'xxx');
			if ( ! $this->connect){
				return "Не удалось подключится к серверу";
			}
			$this->create();
			$this->fill();
		}

		private function create (){
			$sql = "CREATE TABLE 'test'  ('Id'  INT NOT NULL AUTO_INCREMENT PRIMARY KEY," .
			       " 'script_name'  VARCHAR(25)," .
			       " 'start_time'  INT," .
			       " 'end_time'  INT, " .
			       " 'Result'  ENUM('normal','illegal','failed','success'))";
			if ( ! mysqli_query($this->connect, $sql)){
				mysqli_close($this->connect);
				return "Таблицу создать не удалось";
			}

		}

		private function fill (){
			$values = array ();
			for ($i = 0; $i < 100; $i ++){
				$values[] = '(' . '"' . 'script' . rand() . '",' . rand() . ',' . rand() . ',' . rand(1, 4) . ')';
			}

			$insert = implode(',', $values);
			$sql    = "INSERT INTO 'tests' ('script_name', 'start_time', 'end_time', 'Result') VALUES $insert";
			if ( ! mysqli_query($this->connect, $sql)){
				mysqli_close($this->connect);
				return "Таблицу заполнить не удалось";
			}else{
				mysqli_close($this->connect);
			}
		}

		public function get (){
			$this->connect = mysqli_connect('host', 'user', 'password', 'database');
			if ( ! $this->connect){
				return "Не удалось подключится к серверу";
			}else{
				$sql = "SELECT 'Id','script_name','start_time','end_time' FROM 'test' WHERE 'Result = 1' OR 'Result = 4'";
			}
			if ( ! mysqli_query($this->connect, $sql)){
				mysqli_close($this->connect);
				return "Запрос выполнить не удалось";
			}else{
				mysqli_close($this->connect);
				return $sql;
			}
		}
	}