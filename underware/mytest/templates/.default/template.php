<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){
	die();
} ?>
<? if ( ! empty($arResult['ITEMS'])): ?>

    <h3><?=GetMessage("MY_TITLE")?></h3>
	<? foreach ($arResult['ITEMS'] as $item){ ?>
        <p><?=GetMessage("MY_DATE")?> <?=$item['DATE_CREATE']?> / <?=$item['NAME']?> (<?=$item['PROPERTY_ELEMENT_LINK_NAME']?>)</p>
	<? } ?>
<? endif ?>