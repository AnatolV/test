<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("MY_ITEMS_NAME"),
	"DESCRIPTION" => GetMessage("MY_ITEMS_DESC"),
	"ICON" => "/images/menu.gif",
	"PATH" => array(
		"ID" => "utility",
		"CHILD" => array(
			"ID" => "mytest",
			"NAME" => GetMessage("MY_ITEMS_GROUP")
		)
	),
);

?>