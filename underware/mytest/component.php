<?
	if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){
		die();
	}

	if (isset($arParams["MAX_ELEMENTS"]) && 1 < intval($arParams["MAX_ELEMENTS"])){
		$arParams["MAX_ELEMENTS"] = intval($arParams["MAX_ELEMENTS"]);
	}else{
		$arParams["MAX_ELEMENTS"] = 2;
	}
	if (isset($arParams["ELEMENT_ID"]) && ! ! intval($arParams["ELEMENT_ID"])){
		$element_id = intval($arParams["ELEMENT_ID"]);
	}else{
		exit();
	}

	if ($this->startResultCache()){

		$this->getLinkedItems();
		$this->getSelectedItems();
		$this->sortByDate();
	}
	$this->IncludeComponentTemplate();