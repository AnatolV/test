<?
	if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){
		die();
	}
	if ( ! CModule::IncludeModule("iblock")){
		return;
	}

	$arTypesEx = CIBlockParameters::GetIBlockTypes(array ("-" => " "));
	$arIBlocks = array ();
	$db_iblock = CIBlock::GetList(array ("SORT" => "ASC"), array (
		"SITE_ID" => $_REQUEST["site"],
		"TYPE"    => ($arCurrentValues["IBLOCK_TYPE"] != "-"? $arCurrentValues["IBLOCK_TYPE"]: "")
	));
	while ($arRes = $db_iblock->Fetch()){
		$arIBlocks[ $arRes["ID"] ] = "[" . $arRes["ID"] . "] " . $arRes["NAME"];
	}

	$arSorts      = array ("ASC" => GetMessage("T_IBLOCK_DESC_ASC"), "DESC" => GetMessage("T_IBLOCK_DESC_DESC"));
	$arSortFields = array (
		"ID"          => GetMessage("T_IBLOCK_DESC_FID"),
		"NAME"        => GetMessage("T_IBLOCK_DESC_FNAME"),
		"ACTIVE_FROM" => GetMessage("T_IBLOCK_DESC_FACT"),
		"SORT"        => GetMessage("T_IBLOCK_DESC_FSORT"),
		"TIMESTAMP_X" => GetMessage("T_IBLOCK_DESC_FTSAMP")
	);

	$arProperty_LNS = array ();
	$rsProp         = CIBlockProperty::GetList(array ("sort" => "asc", "name" => "asc"), array (
		"ACTIVE"    => "Y",
		"IBLOCK_ID" => (isset($arCurrentValues["IBLOCK_ID"])? $arCurrentValues["IBLOCK_ID"]: $arCurrentValues["ID"])
	));
	while ($arr = $rsProp->Fetch()){
		$arProperty[ $arr["CODE"] ] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
			$arProperty_LNS[ $arr["CODE"] ] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
	}
	$arComponentParameters = array (
		"GROUPS"     => array (
			"CACHE_SETTINGS" => array (
				"NAME" => GetMessage("COMP_GROUP_CACHE_SETTINGS"),
				"SORT" => 600
			),
		),
		"PARAMETERS" => array (

			"IBLOCK_TYPE" => array (
				"PARENT"  => "BASE",
				"NAME"    => GetMessage("MY_IBLOCK_DESC_LIST_TYPE"),
				"TYPE"    => "LIST",
				"VALUES"  => $arTypesEx,
				"DEFAULT" => "news",
				"REFRESH" => "Y",
			),
			"IBLOCK_ID"   => array (
				"PARENT"            => "BASE",
				"NAME"              => GetMessage("MY_IBLOCK_DESC_LIST_ID"),
				"TYPE"              => "LIST",
				"VALUES"            => $arIBlocks,
				"DEFAULT"           => '={$_REQUEST["ID"]}',
				"ADDITIONAL_VALUES" => "Y",
				"REFRESH"           => "Y",
			),
			"ELEMENT_ID"   => array (
				"PARENT"            => "BASE",
				"NAME"              => GetMessage("MY_IBLOCK_DESC_EL_ID"),
				"TYPE"              => "STRING",
				"VALUES"            => '={$arResult["ID"]}',
				"DEFAULT"           => '={$arResult["ID"]}',
			),
			"PROPERTY_CODE" => array (
				"PARENT"            => "DATA_SOURCE",
				"NAME"              => GetMessage("MY_IBLOCK_PROPERTY"),
				"TYPE"              => "LIST",
				"MULTIPLE"          => "Y",
				"VALUES"            => $arProperty_LNS,
				"ADDITIONAL_VALUES" => "Y",
			),
			"MAX_ELEMENTS"  => Array (
				"NAME"    => GetMessage("MAX_ELEMENT"),
				"TYPE"    => "STRING",
				"DEFAULT" => '3',
				"PARENT"  => "ADDITIONAL_SETTINGS",

			),

			"MY_CACHE_TYPE" => array (
				"PARENT"            => "CACHE_SETTINGS",
				"NAME"              => GetMessage("COMP_PROP_CACHE_TYPE"),
				"TYPE"              => "LIST",
				"VALUES"            => array (
					"A" => GetMessage("COMP_PROP_CACHE_TYPE_AUTO"),
					"Y" => GetMessage("COMP_PROP_CACHE_TYPE_YES"),
					"N" => GetMessage("COMP_PROP_CACHE_TYPE_NO"),
				),
				"DEFAULT"           => "N",
				"ADDITIONAL_VALUES" => "N",
			),

			"MY_CACHE_TIME" => array (
				"PARENT"   => "CACHE_SETTINGS",
				"NAME"     => GetMessage("COMP_PROP_CACHE_TIME"),
				"TYPE"     => "STRING",
				"MULTIPLE" => "N",
				"DEFAULT"  => 3600,
				"COLS"     => 5,
			),

		)
	);
?>
