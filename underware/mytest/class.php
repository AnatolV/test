<? if ( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){
	die();
}

	class CBitrixTestComponent extends CBitrixComponent{
		public function onPrepareComponentParams ($arParams){
			$arParams["CACHE_TYPE"] = $arParams["MY_CACHE_TYPE"];
			$arParams["CACHE_TIME"] = $arParams["MY_CACHE_TIME"];

			return $arParams;
		}

		public function getLinkedItems (){

			$db_elements = CIBlockElement::GetProperty(
				IntVal($this->arParams['IBLOCK_ID']),
				IntVal($this->arParams['ELEMENT_ID']),
				"sort", "asc",
				array ("CODE" => "ELEMENT_LINK")
			);
			$LinkedIds   = array ();
			while ($id = $db_elements->GetNext()){
				$LinkedIds[] = $id['VALUE'];
			}

			$this->arResult['LinkedIds'] = $LinkedIds;
		}

		public function getSelectedItems (){
			if ( ! empty($this->arResult['LinkedIds'])){
				$arFilter                = array ("IBLOCK_ID" => $this->arParams['IBLOCK_ID']);
				$arFilter["ACTIVE_DATE"] = "Y";
				$arFilter["ACTIVE"]      = "Y";
				$arSelect                = array ("ID", "NAME", "DATE_CREATE", "PROPERTY_ELEMENT_LINK.NAME");
				foreach ($this->arResult['LinkedIds'] as $link){
					$db_elements = CIBlockElement::GetList(
						array ('ACTIVE_DATE' => 'desc'),
						array (
							"IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
							array (
								"LOGIC" => "AND",
								array (
									"=PROPERTY_ELEMENT_LINK" => intval($link),
									"!ID"                    => $this->arParams['ELEMENT_ID']
								)
							)
						),
						false,
						false,
						$arSelect
					);
					while ($items = $db_elements->GetNext()){
						$this->arResult['ITEMS'][ $items['ID'] ] = $items;
					}
				}

			}
		}

		public function sortByDate (){


			function cmp ($a, $b){
				if ($a["DATE_CREATE"] == $b["DATE_CREATE"]){
					return 0;
				}

				return (strtotime($a["DATE_CREATE"]) > strtotime($b["DATE_CREATE"]))? - 1: 1;
			}

			usort($this->arResult['ITEMS'], "cmp");


		}
	}
